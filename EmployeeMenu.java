package Prac;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EmployeeMenu {
    public static void main(String[] args) throws IOException {
        Employee employee=new Employee();
        FullTimeEmployee fullTimeEmployee=new FullTimeEmployee();
        PartTimeEmployee partTimeEmployee=new PartTimeEmployee();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the options you want");
        System.out.println("1 for adding Employee");
        System.out.println("2 for displaying the tables");
        System.out.println("3 For deleting by id");
        System.out.println("4 For updating by id");
        System.out.println("For diplaying greater than salary");


        while(true)
        {
            System.out.println("Enter the choice");
            int choice=Integer.parseInt(bufferedReader.readLine());
            switch(choice)
            {
                case 1:
                    employee.addEmployee(bufferedReader);
                    System.out.println("Which type of employee you want to add?");
                    System.out.println("1 for Fulltime");
                    System.out.println("2 for Parttime");
                    int op=Integer.parseInt(bufferedReader.readLine());
                    if(op==1)
                        fullTimeEmployee.addFullTimeEmployee(bufferedReader);
                    else if(op==2)
                        partTimeEmployee.addPartTimeEmployee(bufferedReader);

                    break;

                case 2:
                    employee.displayEmployee();
                    partTimeEmployee.displayPartTimeEmployee();
                    fullTimeEmployee.displayFullTimeEmployee();
                    break;

                case 3:
                    employee.deleteEmployee(bufferedReader);
                    break;

                case 4:
                    employee.updateEmployeeName(bufferedReader);
                    break;

                case 5:
                    fullTimeEmployee.fullgreater();
                    partTimeEmployee.partgreater();

            }
        }
    }
}




/*OUTPUT:
C:\Users\Coditas\.jdks\corretto-1.8.0_342\bin\java.exe "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2\lib\idea_rt.jar=64498:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2\bin" -Dfile.encoding=UTF-8 -classpath C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\charsets.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\access-bridge-64.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\cldrdata.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\dnsns.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\jaccess.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\jfxrt.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\localedata.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\nashorn.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunec.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunjce_provider.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunmscapi.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunpkcs11.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\zipfs.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jce.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jfr.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jfxswt.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jsse.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\management-agent.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\resources.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\rt.jar;C:\Users\Coditas\IdeaProjects\JDBC\out\production\JDBC;C:\Users\Coditas\Downloads\mysql-connector-java-8.0.30.jar Prac.EmployeeMenu
Enter the options you want
1 for adding Employee
2 for displaying the tables
3 For deleting by id
4 For updating by id
For diplaying greater than salary
Enter the choice
1
Enter the id of employee
10
Enter the name of the employee
arrtiii
Which type of employee you want to add?
1 for Fulltime
2 for Parttime
1
Enter the fixed salary of the employee
1000
Enter the extra working hours of the employee
45
Enter the salary per hour of the employee
100
Enter the id again
10
Enter the choice
2
THe employee details are
   EMPLOYEEID EMPLOYEE NAME
            1       Sanyami
            2        Ashraf
            3          Aman
            4       Praveer
           10       arrtiii

THe parttimeemployee details are
 EMPLOYEENAME     NOOFHOURS SALARYPERHOUR    EMPLOYEEID   TOTALSALARY
         Aman             3             7           100           700
      Praveer             4             4           700          2800

THe fulltimeemployee details are
 EMPLOYEENAME    EMPLOYEEID   FIXEDSALARY     NOOFHOURS SALARYPERHOUR  TOOTALSALARY
      Sanyami             1          1000             2           100            200
       Ashraf             2          4000             4           100            400
      arrtiii            10          1000            45           100           5500

Enter the choice
3
Enter the id of the employee ypu want to delete
10
Enter the choice
2
THe employee details are
   EMPLOYEEID EMPLOYEE NAME
            1       Sanyami
            2        Ashraf
            3          Aman
            4       Praveer

THe parttimeemployee details are
 EMPLOYEENAME     NOOFHOURS SALARYPERHOUR    EMPLOYEEID   TOTALSALARY
         Aman             3             7           100           700
      Praveer             4             4           700          2800

THe fulltimeemployee details are
 EMPLOYEENAME    EMPLOYEEID   FIXEDSALARY     NOOFHOURS SALARYPERHOUR  TOOTALSALARY
      Sanyami             1          1000             2           100            200
       Ashraf             2          4000             4           100            400

Enter the choice
4
Enter the id of the employee you want to update name of
3
Enter the new name you want
Rajjjj
Enter the choice
2
THe employee details are
   EMPLOYEEID EMPLOYEE NAME
            1       Sanyami
            2        Ashraf
            3        Rajjjj
            4       Praveer

THe parttimeemployee details are
 EMPLOYEENAME     NOOFHOURS SALARYPERHOUR    EMPLOYEEID   TOTALSALARY
       Rajjjj             3             7           100           700
      Praveer             4             4           700          2800

THe fulltimeemployee details are
 EMPLOYEENAME    EMPLOYEEID   FIXEDSALARY     NOOFHOURS SALARYPERHOUR  TOOTALSALARY
      Sanyami             1          1000             2           100            200
       Ashraf             2          4000             4           100            400

Enter the choice
5
THe fulltimeemployee details whose salary is greater than 250 are
 EMPLOYEENAME    EMPLOYEEID   FIXEDSALARY     NOOFHOURS SALARYPERHOUR  TOOTALSALARY
       Ashraf             2          4000             4           100            400

THe parttimeemployee details  with salary 1000 are
 EMPLOYEENAME     NOOFHOURS SALARYPERHOUR    EMPLOYEEID   TOTALSALARY
      Praveer             4             4           700          2800

 */