package Prac;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;
import java.util.Formatter;

public class Employee {

    int employeeId;
    String name;
    Connection connection;
    Statement statement;
    ResultSet resultSet;
    PreparedStatement preparedStatement;

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    void addEmployee(BufferedReader bufferedReader) throws IOException {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            preparedStatement=connection.prepareStatement("Insert into employee values(?,?);");
            System.out.println("Enter the id of employee");
            employeeId=Integer.parseInt(bufferedReader.readLine());
            setEmployeeId(employeeId);
            preparedStatement.setInt(1,getEmployeeId());
            System.out.println("Enter the name of the employee");
            name= bufferedReader.readLine();
            setName(name);
            preparedStatement.setString(2,getName());
            preparedStatement.execute();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

    void displayEmployee()
    {
        Formatter formatter=new Formatter();
        System.out.println("THe employee details are");
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            statement= connection.createStatement();
            resultSet=statement.executeQuery("Select * from employee");
            formatter.format("%13s %13s \n","EMPLOYEEID","EMPLOYEE NAME");
            while(resultSet.next())
            {
                formatter.format("%13s %13s \n",resultSet.getInt(1),resultSet.getString(2));
            }
            System.out.println(formatter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

    void deleteEmployee(BufferedReader bufferedReader) throws IOException {
        System.out.println("Enter the id of the employee ypu want to delete");
        int id=Integer.parseInt(bufferedReader.readLine());
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            statement= connection.createStatement();
            statement.execute("delete from employee where eid="+id+";");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }


    void updateEmployeeName(BufferedReader bufferedReader) throws IOException {

        try {
            System.out.println("Enter the id of the employee you want to update name of");
            int id=Integer.parseInt(bufferedReader.readLine());
            System.out.println("Enter the new name you want");
            String ename=bufferedReader.readLine();
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            statement= connection.createStatement();
            statement.execute("update employee set ename='"+ename+"' where eid="+id+";");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }


    void query()
    {
        Formatter formatter=new Formatter();
        System.out.println("THe employee details whose salary is above 25000 are");
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            statement= connection.createStatement();
            resultSet=statement.executeQuery("Select * from employee");
            formatter.format("%13s %13s \n","EMPLOYEEID","EMPLOYEE NAME");
            while(resultSet.next())
            {
                formatter.format("%13s %13s \n",resultSet.getInt(1),resultSet.getString(2));
            }
            System.out.println(formatter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

class PartTimeEmployee extends Employee
{
    int noOfHours;
    int salaryPerHour;

    public int getNoOfHours() {
        return noOfHours;
    }

    public void setNoOfHours(int noOfHours) {
        this.noOfHours = noOfHours;
    }

    public int getSalaryPerHour() {
        return salaryPerHour;
    }

    public void setSalaryPerHour(int salaryPerHour) {
        this.salaryPerHour = salaryPerHour;
    }

    void addPartTimeEmployee(BufferedReader bufferedReader) throws IOException {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            preparedStatement = connection.prepareStatement("Insert into parttimeemployee(noofhours,salaryperhour,eid,totalsalary ) values(?,?,?,?);");


            System.out.println("Enter the working hours of the employee");
            noOfHours = Integer.parseInt(bufferedReader.readLine());
            setNoOfHours(noOfHours);
            preparedStatement.setInt(1, getNoOfHours());


            System.out.println("Enter the salary per hour of the employee");
            salaryPerHour = Integer.parseInt(bufferedReader.readLine());
            setSalaryPerHour(salaryPerHour);
            preparedStatement.setInt(2, getSalaryPerHour());

            System.out.println("Enter the id again");
            int id=Integer.parseInt(bufferedReader.readLine());
            preparedStatement.setInt(3,id);

            preparedStatement.setInt(4,salaryPerHour*noOfHours);


            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    void displayPartTimeEmployee()
    {
        Formatter formatter=new Formatter();
        System.out.println("THe parttimeemployee details are");
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            statement= connection.createStatement();
            resultSet=statement.executeQuery("Select employee.ename,employee.eid,parttimeemployee.noofhours,parttimeemployee.salaryperhour,parttimeemployee.totalsalary from employee inner join parttimeemployee on employee.eid=parttimeemployee.eid;");
            formatter.format("%13s %13s %13s %13s %13s\n","EMPLOYEENAME","NOOFHOURS","SALARYPERHOUR","EMPLOYEEID","TOTALSALARY");
            while(resultSet.next())
            {
                formatter.format("%13s %13s %13s %13s %13s \n",resultSet.getString(1),resultSet.getInt(2),resultSet.getInt(3),resultSet.getInt(4),resultSet.getInt(5));
            }
            System.out.println(formatter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }



    void partgreater()
    {
        Formatter formatter=new Formatter();
        System.out.println("THe parttimeemployee details  with salary 1000 are");
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            statement= connection.createStatement();
            resultSet=statement.executeQuery("Select employee.ename,employee.eid,parttimeemployee.noofhours,parttimeemployee.salaryperhour,parttimeemployee.totalsalary from employee inner join parttimeemployee on employee.eid=parttimeemployee.eid where parttimeemployee.totalsalary>1000;");
            formatter.format("%13s %13s %13s %13s %13s\n","EMPLOYEENAME","NOOFHOURS","SALARYPERHOUR","EMPLOYEEID","TOTALSALARY");
            while(resultSet.next())
            {
                formatter.format("%13s %13s %13s %13s %13s \n",resultSet.getString(1),resultSet.getInt(2),resultSet.getInt(3),resultSet.getInt(4),resultSet.getInt(5));
            }
            System.out.println(formatter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }
}





class FullTimeEmployee extends Employee {
    int noOfHours;
    int salaryPerHour;
    int fixedSalary;

    public int getNoOfHours() {
        return noOfHours;
    }

    public void setNoOfHours(int noOfSalary) {
        this.noOfHours = noOfHours;
    }

    public int getSalaryPerHour() {
        return salaryPerHour;
    }

    public void setSalaryPerHour(int salaryPerHour) {

        this.salaryPerHour = salaryPerHour;
    }

    public int getFixedSalary() {
        return fixedSalary;
    }

    public void setFixedSalary(int fixedSalary) {
        this.fixedSalary = fixedSalary;
    }

    void addFullTimeEmployee(BufferedReader bufferedReader) throws IOException {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            preparedStatement = connection.prepareStatement("Insert into fulltimeemployee(fixedsalary,noofhours,salaryperhour,eid,totalsalary ) values(?,?,?,?,?);");
            System.out.println("Enter the fixed salary of the employee");
            fixedSalary = Integer.parseInt(bufferedReader.readLine());
            setFixedSalary(fixedSalary);
            preparedStatement.setInt(1, getFixedSalary());

            System.out.println("Enter the extra working hours of the employee");
            noOfHours = Integer.parseInt(bufferedReader.readLine());
            setNoOfHours(noOfHours);
            preparedStatement.setInt(2, getNoOfHours());


            System.out.println("Enter the salary per hour of the employee");
            salaryPerHour = Integer.parseInt(bufferedReader.readLine());
            setSalaryPerHour(salaryPerHour);
            preparedStatement.setInt(3, getSalaryPerHour());

            System.out.println("Enter the id again");
            int id=Integer.parseInt(bufferedReader.readLine());
            preparedStatement.setInt(4,id);

            preparedStatement.setInt(5,+fixedSalary+(salaryPerHour*noOfHours));



            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    void displayFullTimeEmployee()
    {
        Formatter formatter=new Formatter();
        System.out.println("THe fulltimeemployee details are");
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            statement= connection.createStatement();
            resultSet=statement.executeQuery("Select employee.ename,employee.eid,fulltimeemployee.fixedsalary,fulltimeemployee.noofhours,fulltimeemployee.salaryperhour,fulltimeemployee.totalsalary from employee inner join fulltimeemployee on employee.eid=fulltimeemployee.eid;");


            formatter.format("%13s %13s %13s %13s %13s %13s \n","EMPLOYEENAME","EMPLOYEEID","FIXEDSALARY","NOOFHOURS","SALARYPERHOUR","TOOTALSALARY");
            while(resultSet.next())
            {
                formatter.format("%13s %13s %13s %13s %13s  %13s\n",resultSet.getString(1),resultSet.getInt(2),resultSet.getInt(3),resultSet.getInt(4),resultSet.getInt(5),resultSet.getInt(6));
            }
            System.out.println(formatter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

    void fullgreater()
    {
        Formatter formatter=new Formatter();
        System.out.println("THe fulltimeemployee details whose salary is greater than 250 are");
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/bookauthor", "root", "Root@12");
            statement= connection.createStatement();
            resultSet=statement.executeQuery("Select employee.ename,employee.eid,fulltimeemployee.fixedsalary,fulltimeemployee.noofhours,fulltimeemployee.salaryperhour,fulltimeemployee.totalsalary from employee inner join fulltimeemployee on employee.eid=fulltimeemployee.eid where fulltimeemployee.totalsalary>250;");


            formatter.format("%13s %13s %13s %13s %13s %13s \n","EMPLOYEENAME","EMPLOYEEID","FIXEDSALARY","NOOFHOURS","SALARYPERHOUR","TOOTALSALARY");
            while(resultSet.next())
            {
                formatter.format("%13s %13s %13s %13s %13s  %13s\n",resultSet.getString(1),resultSet.getInt(2),resultSet.getInt(3),resultSet.getInt(4),resultSet.getInt(5),resultSet.getInt(6));
            }
            System.out.println(formatter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }
}


